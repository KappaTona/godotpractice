# the tool keyword is useful to run the script from editor
# so no scene play is necessary
@tool
extends Node3D

# the gridmap reference points to the child gridmap in this node.
@onready var grid_map: GridMap = $GridMap

# export var so that it shows up TR
@export var gen_level: bool = false : set = set_gen_level

# resets the generated cells from editor
@export var reset_grid_map: bool = false : set = call_reset_grid_map
func call_reset_grid_map(_value:bool=false):
	grid_map.clear()
	
@export_multiline var dungeon_seed: String = "" : set = set_seed
func set_seed(value:String):
	dungeon_seed = value
	seed(value.hash())
	
# generate level button, using the checkbox flip so that
# everytime it is clicked the generate_script will be called
func set_gen_level(_value: bool):
	if Engine.is_editor_hint():
		generate_level()

# chance of adding back a delone point to the minimum spanning graph
# so that the layout is not always the shortest distance based graph.
# TODO: remove this comment: survival_chance
@export_range(0, 1) var noise_for_halway: float = 0.25
# the boundary of the level
@export var border_size: int = 20 : set = set_border_size
func set_border_size(value: int):
	border_size = value
	# for future: only call function after 1st frame is ready
	# for now: this will be called from editor I suppose
	if Engine.is_editor_hint():
		# make the borders visible
		make_borders_visible()


# TODO: when the value is negative "nothing" happens
func make_borders_visible():
	call_reset_grid_map()
	for i in range(-1, border_size+1):
		grid_map.set_cell_item(Vector3i(i, 0, -1), 3)
		grid_map.set_cell_item(Vector3i(i, 0, border_size), 3)
		grid_map.set_cell_item(Vector3i(border_size, 0, i), 3)
		grid_map.set_cell_item(Vector3i(-1, 0, i), 3)

func generate_level():
	make_borders_visible()
	room_tiles.clear()
	room_positions_on_level.clear()
	if dungeon_seed: set_seed(dungeon_seed)
	for i in room_number:
		make_room(room_recursion)

	# delaunay or delone triangulation algorythm
	# https://en.wikipedia.org/wiki/Delaunay_triangulation
	var room_positions_delone: PackedVector2Array = []
	var delone_graph:= AStar2D.new()
	var minimum_spanning_graph:= AStar2D.new()
	 
	for pos in room_positions_on_level:
		var position_vector:= Vector2(pos.x, pos.z)
		room_positions_delone.append(position_vector)
		
		# placing the points
		delone_graph.add_point(
			delone_graph.get_available_point_id(), position_vector)
		minimum_spanning_graph.add_point(
			minimum_spanning_graph.get_available_point_id(), position_vector)
			
	var delone_array:= Array(
		Geometry2D.triangulate_delaunay(room_positions_delone))
	
	var triangle_count: int = delone_array.size()/3
	for i in triangle_count:
		var p1: int = delone_array.pop_front()
		var p2: int = delone_array.pop_front()
		var p3: int = delone_array.pop_front()
		delone_graph.connect_points(p1, p2)
		delone_graph.connect_points(p2, p3)
		delone_graph.connect_points(p1, p3)
		
	# visiting is approached by their index, not their positions
	var visited_points: PackedInt32Array = []
	visited_points.append(randi_range(0, room_positions_on_level.size()-1))
	while visited_points.size() != minimum_spanning_graph.get_point_count():
		# recalculate the position candidates
		var possible_connection: Array[PackedInt32Array] = []
		for vp in visited_points:
			for connection in delone_graph.get_point_connections(vp):
				if !visited_points.has(connection):
					var connection_point: PackedInt32Array = [vp, connection]
					possible_connection.append(connection_point)
		
		var picked_connection: PackedInt32Array = possible_connection.pick_random()
		for pc in possible_connection:
			var next_pos:= room_positions_delone[pc[0]].distance_squared_to(
				room_positions_delone[pc[1]])
			var current_pos:= room_positions_delone[picked_connection[0]].distance_squared_to(
				room_positions_delone[picked_connection[1]])
			if  next_pos < current_pos:
				picked_connection = pc
		
		visited_points.append(picked_connection[1])
		minimum_spanning_graph.connect_points(picked_connection[0], picked_connection[1])
		delone_graph.disconnect_points(picked_connection[0], picked_connection[1])
		
	var hallway_grapgh: AStar2D = minimum_spanning_graph
	# At this point some of the graph points had been removed because of
	# the minimum span graph
	for points in delone_graph.get_point_ids():
		for connection in delone_graph.get_point_connections(points):
			# avoid running on same connection
			if connection > points:
				var removal_chance:= randf()
				if noise_for_halway > removal_chance:
					hallway_grapgh.connect_points(points, connection)
					
	create_hallways(hallway_grapgh)
				
func create_hallways(hallway_graph: AStar2D):
	var hallways: Array[PackedVector3Array] = []
	# TODO this seems familiar
	for points in hallway_graph.get_point_ids():
		for connection in hallway_graph.get_point_connections(points):
			# avoid runningf on same connection
			if connection > points:
				var room_from: PackedVector3Array = room_tiles[points]
				var room_to: PackedVector3Array = room_tiles[connection]
				var tile_from:= room_from[0]
				var tile_to:= room_to[0]
				# TODO: possible swap?
				for tile in room_from:
					if tile.distance_squared_to(
						room_positions_on_level[connection]) < tile_from.distance_squared_to(
							room_positions_on_level[connection]):
								tile_from = tile
				# exact same :()				
				for tile in room_to:
					if tile.distance_squared_to(
						room_positions_on_level[points]) < tile_to.distance_squared_to(
							room_positions_on_level[points]):
								tile_to = tile
								
				var hallway: PackedVector3Array = [tile_from, tile_to]
				hallways.append(hallway)
				# setting doors
				grid_map.set_cell_item(tile_from, 2)
				grid_map.set_cell_item(tile_to, 2)
	
	# path finding
	# TODO: remove this comment, ommited Astar2DGrid type
	var pathfind_a_star = AStarGrid2D.new()
	pathfind_a_star.size = Vector2i.ONE * border_size
	pathfind_a_star.update()
	pathfind_a_star.diagonal_mode = AStarGrid2D.DIAGONAL_MODE_NEVER
	pathfind_a_star.default_estimate_heuristic = AStarGrid2D.HEURISTIC_MANHATTAN
	
	# making room tiles of the grid map solid so it can not be passed
	for t in grid_map.get_used_cells_by_item(0):
		pathfind_a_star.set_point_solid(Vector2i(t.x, t.z))
		
	for h in hallways:
		var pos_from:= Vector2i(h[0].x, h[0].z)
		var pos_to:= Vector2i(h[1].x, h[1].z)
		var hallway:= pathfind_a_star.get_point_path(pos_from, pos_to)
		
		for t in hallway:
			var pos:= Vector3i(t.x, 0, t.y)
			# if the cell item empty
			if grid_map.get_cell_item(pos) < 0:
				# mesh index of 1 = hallway
				grid_map.set_cell_item(pos, 1)

# room parameters
@export var room_number: int = 7
@export var room_recursion: int = 14
# minimum distance for each roomright
@export var room_margin: int = 1
@export var min_room_size: int = 2
@export var max_room_size: int = 4
var room_tiles: Array[PackedVector3Array] = []
var room_positions_on_level: PackedVector3Array = []
# TODO: kinda review what is going on here?
# something is fishy with the rani_range. I replaced the module random with
# randi_range and kinda substracted the width, height from start position
# instead of themself. It just works for the moment.
func make_room(recursion_limit:int):
	# if limit: no more room generation.
	if recursion_limit <= 0:
		return

	var width: int = randi_range(min_room_size, max_room_size)
	var height: int = randi_range(min_room_size, max_room_size)

	var start_position:= Vector3i(
		randi_range(1, border_size-width-1), 0, randi_range(1, border_size-height-1) )

	# TODO: refactor naming, splitting you know the drill
	# create rooms from left to right
	for row in range(-room_margin, height-room_margin):
		for column in range(-room_margin, width-room_margin):
			var room_tile_pos1:= start_position + Vector3i(column, 0, row)
			# the 0 is the mesh index of the room tile
			if grid_map.get_cell_item(room_tile_pos1) == 0:
				make_room(recursion_limit-1)
				return

	# TODO deep dive this
	var room : PackedVector3Array = []
	for row in height:
		for column in width:
			var room_tile_pos1 := start_position + Vector3i(column, 0, row)
			grid_map.set_cell_item(room_tile_pos1,0)
			room.append(room_tile_pos1)
	room_tiles.append(room)
	var avg_x : float = start_position.x + (float(width)/2)
	var avg_z : float = start_position.z + (float(height)/2)
	var room_tile_pos := Vector3(avg_x,0,avg_z)
	room_positions_on_level.append(room_tile_pos)
