Based on [this awesome tutorial](https://www.youtube.com/watch?v=h64U6j_sFgs) I have decided to practice Godot 4 for fun.

#### TODO
- there is something fishy in the random generation. Somehow the rooms are overlapping but not as bad as some would think. Deep dive later.

I have changed the randi() % to randi\_range and the math does not check out.
```python
	var width: int = randi_range(min_room_size, max_room_size)
	var height: int = randi_range(min_room_size, max_room_size)

	var start_position: Vector3i
	start_position.x = randi_range(width, border_size-width)
	start_position.z = randi_range(height, border_size-height)

	# create rooms from left to right
	for row in range(-room_margin, height-room_margin):
		for column in range(-room_margin, width-room_margin):
```	

- use combination of minimum spanning graph and delone graph to make more insteresting layout. the exported float range (0,1) named `noise_for_hallway` is used to put back connection points to the delone graph these points might have been removed due to the minimum spanning graph algorythm.
